from setuptools import setup, find_packages

setup(
    name='thetaleapi',
    version='alpha',
    description='Python module for communicating with the-tale.org',
    classifiers=[
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.4',
    ],
    author='Nikolay Aleksandrovich Pavlov',
    author_email='kp-pav@yandex.ru',
    url='https://bitbucket.org/ZyX_I/thetalebot',
    license='MIT',
    keywords='',
    packages=find_packages(exclude=('tests', 'tests.*')),
    extras_require={
        'docs': [
            'Sphinx',
        ],
    },
    test_suite='tests',
)
