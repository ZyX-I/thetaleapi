let g:syntastic_python_pylint_args='--disable=too-few-public-methods,trailing-whitespace,fixme,too-many-instance-attributes,invalid-name,too-many-arguments,star-args'
let g:syntastic_python_checkers=['pylint']
setlocal expandtab
