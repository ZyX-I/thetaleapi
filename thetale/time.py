'''Module that describes internal game time
'''

from .util import SlotRepr

import re


class TheTaleTime(SlotRepr):
    '''In-game time
    '''
    __slots__ = ('string', 'hours', 'minutes')

    def __init__(self, time_string):
        self.string = time_string
        '''Original string, unmodified.'''
        hs, ms = time_string.split(':')
        self.hours = int(hs)
        '''Hours, integer.'''
        self.minutes = int(ms)
        '''Minutes, integer.'''


DATE_RE = re.compile(
    r'^(?P<day>\d+) (?P<month>\S+) месяц. (?P<year>\d+) год.?$')
'''Regular expression used to parse the-tale.org dates.'''
MONTHS_GENITIVE_CASE_TO_SUBJECTIVE_CASE = {
    'холодного': 'холодный',
    'сырого': 'сырой',
    'жаркого': 'жаркий',
    'сухого': 'сухой',
}
'''Dictionary mapping month names in genetive case to names in subjective case
'''
MONTHS = {
    'холодный': 1,
    'сырой': 2,
    'жаркий': 3,
    'сухой': 4,
}
'''Dictionary mapping months names (all-lower, subjective) to their order
'''


class TheTaleDate(SlotRepr):
    '''In-game date
    '''
    __slots__ = ('string', 'month_name', 'month', 'year', 'day')

    def __init__(self, date_string):
        self.string = date_string
        '''Original string, unmodified.'''
        match = DATE_RE.match(date_string)
        if match:
            self.day = int(match.group('day'))
            '''Day, integer.'''
            self.month_name = MONTHS_GENITIVE_CASE_TO_SUBJECTIVE_CASE[
                match.group('month')]
            '''Month name, str.'''
            self.year = int(match.group('year'))
            '''Year, integer.'''
            try:
                self.month = MONTHS[self.month_name]
            except KeyError:
                raise ValueError('Unknown month: {0}'.format(self.month_name))
        else:
            raise ValueError('Unknown date format: {0}'.format(date_string))
