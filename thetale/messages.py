'''Module that contains classes describing messages from log and diary
'''

from .util import SlotRepr
from .time import TheTaleTime, TheTaleDate

from datetime import datetime


class Message(SlotRepr):
    '''Class describing one message
    '''
    __slots__ = ('real_time', 'time', 'date', 'message')

    def __init__(self, turn, message_data):
        assert isinstance(message_data, list)
        self.real_time = datetime.fromtimestamp(message_data[0])
        '''Real-word time of the message.'''
        self.time = TheTaleTime(message_data[1])
        '''Internal game time, see :py:class:`thetale.time.TheTaleTime`.'''
        self.message = message_data[2]
        '''Actual message.'''
        try:
            self.date = TheTaleDate(message_data[3])
            '''Internal game date, see :py:class:`thetale.time.TheTaleDate`.'''
        except IndexError:
            self.date = turn.date


class Messages(list, SlotRepr):
    '''Class with messages list
    '''
    def __new__(cls, *_):
        return list.__new__(cls)

    def __init__(self, turn, messages_data):
        super(Messages, self).__init__()
        for message_data in messages_data:
            self.append(Message(turn, message_data))
