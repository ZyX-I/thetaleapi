'''Module that contains classes describing a city
'''

from .util import SlotRepr

from enum import Enum
from datetime import datetime
from collections import defaultdict


class CitySpecializations(Enum):
    '''Possible city specializations
    '''
    shopping_center = 0
    city_of_masters = 1
    fort = 2
    political_center = 3
    city_state = 4
    resort = 5
    traffic_center = 6
    freemen = 7
    sacred_city = 8
    common_city = 9


class CityPosition(SlotRepr):
    '''Class describing city position
    '''
    __slots__ = ('x', 'y')

    def __init__(self, position_data):
        self.x = float(position_data['x'])
        '''X coordinate, floating-point.'''
        self.y = float(position_data['y'])
        '''X coordinate, floating-point.'''


class CityBasicInfo(SlotRepr):
    '''Basic information about one city
    '''
    __slots__ = ('id', 'name', 'frontier', 'position', 'size', 'specialization')

    def __init__(self, city_data):
        self.id = city_data['id']
        '''Unique city identifier.

        Some integer. It is not safe to assume anything else about it.'''
        self.name = city_data['name']
        '''City name, string.'''
        self.frontier = city_data['frontier']
        '''Determines whether city is on frontier, boolean.'''
        self.position = CityPosition(city_data['position'])
        '''Position of the city'''
        try:
            self.size = city_data['size']
            '''City size, positive integer.'''
        except KeyError:
            # TODO: Should be handled later in CityInfo
            pass
        if 'specialization' in city_data:
            self.specialization = (
                CitySpecializations(city_data['specialization'])
                if city_data['specialization']
                else None
            )
            '''City specialization, :py:class:`CitySpecialization` or ``None``.
            '''
        # TODO: else: should be handled later in CityInfo


class CitiesInfo(dict, SlotRepr):
    '''List of cities
    '''
    __slots__ = ('response',)

    def __new__(cls, _):
        return dict.__new__(cls)

    def __init__(self, cities_data):
        super(CitiesInfo, self).__init__()
        self.update((int(key), CityBasicInfo(val))
                    for key, val
                    in cities_data['data']['places'].items())
        self.response = cities_data
        '''Original response: parsed JSON object

        Use of this attribute is highly discouraged. Use it only if data is not 
        accessible by other means.
        '''


class CirclePoliticPower(SlotRepr):
    '''Information about inner/outer circle power
    '''
    __slots__ = ('value', 'fraction')

    def __init__(self, circle_data):
        self.value = circle_data['value']
        '''Influence of the circle, integer.'''
        self.fraction = circle_data['fraction']
        '''Fraction among all other cities, floating-point.

        Frontier and core are counted separately.
        '''


class HeroInfluence(SlotRepr):
    '''Information about single hero influence on the city
    '''
    __slots__ = ('positive', 'negative')

    def __init__(self, positive, negative):
        self.positive = positive
        '''Amount of positive influence, floating-point.'''
        self.negative = negative
        '''Amount of negative influence, floating-point.'''


class CityPolitics(SlotRepr):
    '''Information about city political power
    '''
    __slots__ = ('inner_circle_power', 'outer_circle_power', 'total_fraction',
                 'heroes_influence')

    def __init__(self, politics_data):
        self.inner_circle_power = CirclePoliticPower(
            politics_data['power']['inner'])
        '''Inner circle power, :py:class:`CirclePoliticPower`.'''
        self.outer_circle_power = CirclePoliticPower(
            politics_data['power']['outer'])
        '''Outer circle power, :py:class:`CirclePoliticPower`.'''
        self.total_fraction = politics_data['power']['fraction']
        '''Total fraction among all other cities, floating-point.

        Frontier and core are counted separately.
        '''
        self.heroes_influence = defaultdict(lambda: HeroInfluence(0.0, 0.0))
        for attr in ('positive', 'negative'):
            for k, v in politics_data['heroes'][attr].items():
                setattr(self.heroes_influence[int(k)], attr, v)


class CityInfo(CityBasicInfo):
    '''Complete information about one city
    '''
    __slots__ = ('new_time', 'update_time', 'new', 'description', 'politics',
                 'response')

    def __init__(self, city_data):
        self.response = city_data
        '''Original response: parsed JSON object.

        Use of this attribute is highly discouraged. Use it only if data is not 
        accessible by other means.
        '''
        city_data = city_data['data']
        super(CityInfo, self).__init__(city_data)
        self.new_time = datetime.fromtimestamp(city_data['new_for'])
        '''Time until which city is considered new.'''
        self.update_time = datetime.fromtimestamp(city_data['updated_at'])
        '''Time of the last update of the information.'''
        self.new = (self.new_time < self.update_time)
        '''True if city is to be considered new.'''
        self.description = city_data['description']
        '''Description of the city, string.'''
        self.politics = CityPolitics(city_data['politic_power'])
        '''Information about city political power.'''
        # TODO: masters
        # TODO: attributes
        # TODO: attributes: put size attribute value to .size
        # TODO: demographics
        # TODO: bills
        # TODO: habits
        # TODO: chronicle
        # TODO: accounts
        # TODO: clans
