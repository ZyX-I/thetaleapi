'''Module that contains classes describing fate cards
'''

from .util import SlotRepr

from enum import Enum


class FateCards(Enum):
    '''Possible fate cards
    '''
    # cornucopia = 0 (after 56)

    inspiration = 1

    energy_drop = 5
    forces_bowl = 6
    magical_whirlwind = 7
    energy_storm = 8
    barrage_of_force = 9

    handful_of_coins = 10
    weighty_purse = 11
    trunk_for_luck = 12

    moderation = 13
    gluttony = 14
    peace = 15
    irascibility = 16
    faithfulness = 17
    lechery = 18
    friendliness = 19
    greed = 20
    modesty = 21
    vanity = 22
    discretion = 23
    anger = 24
    humility = 25
    pride = 26
    peacefulness = 27
    rage = 28

    enemy_acquirement = 29
    new_homeland = 30
    new_teammate = 31
    new_opponent = 32
    enlightenment = 33
    tastes_in_gear = 34
    definition_of_bravery = 35
    wearied_property = 36
    combat_style_revision = 37
    new_vision = 95
    sensitivity = 96

    revision_of_valuables = 38

    choice = 39

    strange_itching = 40
    store_push = 41
    aspiration_for_perfection = 42
    thirst_for_knowledge = 43
    concern_about_property = 44
    caring = 105

    fairy_fixer = 45
    great_creator_blessing = 46

    other_concerns = 47

    sudden_discovery = 48
    useful_gift = 49
    rare_acquisition = 50
    custodian_gift = 51

    hand_of_death = 52

    magic_coin = 53
    magic_pot = 54
    magic_tablecloth = 55
    illimitable_wealth = 56
    cornucopia = 0

    magic_tool = 57

    smile_of_fortune = 108
    good_day = 58
    unexpected_benefit = 59
    good_scam = 60
    crime_of_century = 61

    lucrative_contract = 109
    lovely_days = 62
    business_day = 63
    city_holiday = 64
    economic_growth = 65

    broken_contract = 110
    nasty_weather = 66
    desolation = 67
    plague_of_rats = 68
    economic_recession = 69

    error_in_archives = 70
    fake_recommendations = 71
    feast_on_board = 72
    plots = 73

    clever_idea = 74
    pure_reason = 75
    unexpected_complications = 76
    word_of_gzanzar = 77
    gzanzar_blessing = 97

    new_facts = 78
    special_operation = 79
    word_of_dabnglan = 80
    dabnglan_blessing = 81
    ace_in_the_hole = 107

    teleport = 82
    tardis = 83

    amnesia = 84  # Removed
    energy_donor = 85  # Removed
    debt_collection = 86  # Removed
    energy_ritual = 87  # Removed

    magical_grindstone = 88
    essence_of_things = 89

    common_companion = 90
    uncommon_companion = 91
    rare_companion = 92
    epic_companion = 93
    legendary_companion = 94

    # new_vision = 95 (after 37)
    # sensitivity = 96

    # gzanzar_blessing = 97 (after 77)

    new_doors = 98
    four_winds = 99

    respite = 100
    plantain = 101
    sacred_honey = 102
    rejuvenating_apple = 103
    water_of_life = 104

    # caring = 105 (after 44)

    hidden_potential = 106

    # ace_in_the_hole = 107 (after 81)
    # smile_of_fortune = 108 (after 57)
    # lucrative_contract = 109 (after 61)
    # broken_contract = 110 (after 65)

    grimace_of_fortune = 111
    nasty_day = 112
    unexpected_trouble = 113
    disastrous_event = 114
    black_stripe = 115


class FateCardsRarity(Enum):
    '''Possible fate cards rarities

    Rarities can be compared with each other (less/greater comparison).
    '''
    common = (0, 'обычная')
    uncommon = (1, 'необычная')
    rare = (2, 'редкая')
    epic = (3, 'эпическая')
    legendary = (4, 'легендарная')

    def __ge__(self, other):
        if self.__class__ is other.__class__:
            return self.value[0] >= other.value[0]
        return NotImplemented

    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self.value[0] > other.value[0]
        return NotImplemented

    def __le__(self, other):
        if self.__class__ is other.__class__:
            return self.value[0] <= other.value[0]
        return NotImplemented

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value[0] < other.value[0]
        return NotImplemented


COMPANION_FATE_CARDS = {
    FateCards.common_companion,
    FateCards.uncommon_companion,
    FateCards.rare_companion,
    FateCards.epic_companion,
    FateCards.legendary_companion,
}


class FateCardState(SlotRepr):
    '''Description of one fate card
    '''
    __slot__ = ('id', 'type', 'sellable', 'name', 'companion_name')

    def __init__(self, card_data):
        self.id = card_data['uid']
        '''Per-player unique card identifier.

        Some integer. It is not safe to assume anything else about it.'''
        self.type = FateCards(card_data['type'])
        '''Card type, :py:class:`FateCards`.'''
        self.sellable = card_data['auction']
        '''True if card can be sold.'''
        self.name = card_data['name']
        '''Fate card name

        May be used to distinguish different *_companion cards.'''
        if self.type in COMPANION_FATE_CARDS:
            self.companion_name = self.name.partition(': ')[2]
            '''Name of the companion that given card gives

            May be None if card gives no companion.
            '''
        else:
            self.companion_name = None


class FateCardsState(dict, SlotRepr):
    '''Class representing currently present fate cards

    Keys are player-unique card identifiers, values are 
    :py:class:`FateCardState` instances.
    '''
    __slots__ = ('cards_help_count', 'cards_help_barrier')

    def __new__(cls, _):
        return dict.__new__(cls)

    def __init__(self, cards_data):
        super(FateCardsState, self).__init__()
        for card_data in cards_data['cards']:
            self[card_data['uid']] = FateCardState(card_data)
        self.cards_help_count = cards_data['help_count']
        '''Number of times help was triggered since last card acquisition, int.
        '''
        self.cards_help_barrier = cards_data['help_barrier']
        '''Number of times help should be triggered to get new fate card, int.
        '''
