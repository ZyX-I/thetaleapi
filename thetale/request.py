'''Module that implements low-level interaction with the-tale.org

Responsibilities:
* Sending requests to the-tale.org.
* Parsing results.

Entry point: :py:class:`TheTaleClient` class.
'''

import json

from time import sleep
from uuid import uuid4
from copy import deepcopy

from urllib.parse import urlencode
from urllib.request import Request, urlopen
from http.cookiejar import CookieJar

from concurrent.futures import ThreadPoolExecutor
from os import cpu_count

from enum import Enum


class AuthorisationState(Enum):
    '''List of possible authorisation states
    '''
    not_requested = 0
    '''Authorisation was not yet requested'''
    no_decision = 1
    '''User has not yet decided whether he needs to authorise the application
    '''
    authorised = 2
    '''User has authorised the application'''
    authorisation_rejected = 3
    '''User has explicitly rejected access'''


class AuthorisationRejectedError(IOError):
    '''Exception thrown if authorisation was rejected

    Needs user to explicitly reject authorisation request.
    '''
    pass


class AuthorisationTimeOutError(IOError):
    '''Exception thrown if authorisation request timed out
    '''
    pass


class AuthorisationNotRequestedError(IOError):
    '''Exception thrown if authorisation state is checked without request

    Normally it is assumed that first :py:meth:`Client.authorise` is called 
    prior to :py:meth:`Client._check_authorisation_state`. This exception will 
    be thrown if this is not true.
    '''
    pass


class NoPatchTurn(ValueError):
    '''Exception thrown if :py:meth:`Client.info` did not find previous response
    '''
    pass


class TheTaleAPIError(ValueError):
    '''Exception thrown when the-tale.org returns API error
    '''
    code = None
    '''Error code'''
    error = None
    '''Error message'''
    errors = None
    '''Found errors in supplied fields'''

    def __init__(self, code, error=None, errors=None):
        if error is None:
            assert errors
            message = 'Found numerous API errors:\n' + (
                '\n'.join('  In field {field}:\n{errors}'.format(
                    field=field,
                    errors='\n'.join('    ' + msg for msg in error_list),
                ))
                for field, error_list in errors.items()
            )
        else:
            message = error
        super(TheTaleAPIError, self).__init__(message)
        self.code = code
        self.errors = errors
        self.error = error

    def __repr__(self):
        return '{class_name}({code!r}, {error!r}, {errors!r})'.format(
            class_name=self.__class__.__name__,
            **self.__dict__
        )


class Client(object):
    '''Class that implements interaction with the-tale.org

    Does not know anything about exact API methods, except for those used for 
    authorisation.
    '''

    __slots__ = ('_client_id', '_connection', '_sessionid', '_csrftoken',
                 'account', 'executor')

    def __init__(self, sessionid=None, client_id='thetaleapi-dev',
                 executor=None):
        '''
        :param str sessionid:
            Session identifier.
        :param str client_id:
            API client name.
        :param concurrent.futures.Executor executor:
            Executor which will be used for obtaining deferred results. Defaults 
            to :py:class:`concurrent.Futures` with :py:func:`os.cpu_count` 
            maximum number of workers.
        '''
        self._client_id = client_id
        self._connection = None
        self._sessionid = sessionid
        self._csrftoken = uuid4().hex
        self.account = None
        self.executor = executor or ThreadPoolExecutor(cpu_count())

    def _generate_url(self, resource_path, method_name, method_version,
                      **kwargs):
        '''Generate request URL

        :param str resource_path:
            Path to the game resource.
        :param str method_name:
            Name of the method that should be called.
        :param str method_version:
            API version in a form ``1.0``, ``1.1``.
        :param dict kwargs:
            Other arguments. This dictionary must not contain ``api_version`` or 
            ``api_client`` keys.

        :return: Full request url.
        '''
        return ('{resource_path}/api/{method_name}?'
                + 'api_version={method_version}&'
                + 'api_client={client_id}&'
                + '{method_arguments}').format(
                    resource_path=resource_path,
                    method_name=method_name,
                    method_version=method_version,
                    client_id=self._client_id,
                    method_arguments=urlencode(kwargs, safe=','),
                )

    def _get_deferred_result(self, status_url, interval=1):
        '''Get result for request with ``deferred`` status

        :param str status_url:
            URL which should be requested.
        :param int interval:
            Query interval.

        :return:
            :py:class:`concurrent.futures.Future` which will eventually return 
            parsed JSON object with the request result.
        '''
        while True:
            jobj = self._parse_http_response(*self._get_http_response(
                'GET', status_url))
            if jobj['status'] == 'ok':
                return jobj
            sleep(interval)

    def _get_http_response(self, method, url, data=None, headers=None):
        '''Get HTTP response, reusing existing HTTP connection if possible

        :param str method:
            Method used for obtaining data (``GET`` or ``POST``).
        :param str url:
            Request URL.
        :param dict headers:
            Additional headers if any.
        '''
        headers = headers.copy() if headers else {}
        headers.update({
            b'Cookie': (
                (
                    (
                        'sessionid={0}; '.format(self._sessionid)
                    ) if self._sessionid else
                    ''
                ) + (
                    'csrftoken={0}'.format(self._csrftoken)
                )
            ).encode('ascii'),
            b'X-CSRFToken': self._csrftoken.encode('ascii'),
        })
        req = Request('http://the-tale.org' + url, data=data, headers=headers,
                      method=method)
        res = urlopen(req)
        return req, res

    def _parse_http_response(self, req, http_response):
        '''Parse HTTPResponse object

        Extracts cookies, saving sessionid to :py:class:`Client` object instance 
        state and parses response text as JSON.

        :param HTTPResponse http_response:
            Parsed object.
        :param str url:
            Requested URL.

        :return:
            Parsed JSON object with the request result. May raise 
            :py:class:`thetale.request.TheTaleAPIError` if API call exited with 
            error.
        '''
        cjar = CookieJar()
        cjar.extract_cookies(http_response, req)
        for cookie in cjar:
            if cookie.name == 'sessionid':
                self._sessionid = cookie.value
                break

        jstr = http_response.read()
        jstr = jstr.decode('utf-8')
        jobj = json.loads(jstr)
        if jobj['status'] == 'error':
            raise TheTaleAPIError(jobj['code'],
                                  jobj.get('error'),
                                  jobj.get('errors'))
        else:
            return jobj

    def request(self, *, method='GET', resource_path, method_name,
                method_version, data_in_url=None, **kwargs):
        '''Send request to the the-tale.org server

        See :py:meth:`_generate_url` for the list of arguments.

        :param bool data_in_url:
            If it is ``True`` then additional keyword arguments are always 
            included in the URL and not in request body. If it is ``False`` then 
            they are always included in the request body. Default is ``True`` 
            for GET requests and ``False`` for POST.

        :return:
            :py:class:`concurrent.futures.Future` which will eventually return 
            parsed JSON object or this object itself with the request result. 
            May raise :py:class:`thetale.request.TheTaleAPIError` if API call 
            exited with error.
        '''
        if ((method == 'GET'
             and data_in_url is not False) or data_in_url is True):
            url = self._generate_url(resource_path, method_name, method_version,
                                     **kwargs)
            data = None
        else:
            url = self._generate_url(resource_path, method_name, method_version)
            data = urlencode(kwargs, safe=',').encode('ascii')
        jobj = self._parse_http_response(*self._get_http_response(method, url,
                                                                  data))
        if jobj['status'] == 'processing':
            return self.executor.submit(self._get_deferred_result,
                                        jobj['status_url'])
        else:
            return jobj

    def _authorisation_state(self):
        '''Return current authorisation state

        Raw variant, for use in base class. Should not be overridden.
        '''
        return self.request(
            resource_path='/accounts/third-party/tokens',
            method_name='authorisation-state',
            method_version='1.0',
        )

    def authorisation_state(self):
        '''Return current authorisation state
        '''
        return self._authorisation_state()

    def _check_authorisation_state(self):
        '''Request authorisation state

        Mostly useful after calling :py:meth:`authorise` method.

        :return:
            Returns a tuple with two values or raises
            :py:class:`thetale.request.TheTaleAPIError`, 
            :py:class:`thetale.request.AuthorisationNotRequestedError` or 
            :py:class:`thetale.request.AuthorisationRejectedError` exception. 
            Tuple contents:

            #. True if application was authorised, False if it was not yet. 
               Raises an exception if authorisation was not previously requested 
               or if user denied authorisation.
            #. Response, same thing :py:meth:`_authorisation_state` returns.
        '''
        response = self._authorisation_state()
        state = AuthorisationState(response['data']['state'])
        if state == AuthorisationState.authorised:
            return True, response
        if state == AuthorisationState.no_decision:
            return False, response
        if state == AuthorisationState.not_requested:
            raise AuthorisationNotRequestedError()
        if state == AuthorisationState.authorisation_rejected:
            raise AuthorisationRejectedError()
        raise RuntimeError('Unexpected state')

    def authorise(self, request_user_action_callback,
                  application_name='thetaleapi',
                  application_info='thetaleapi Python library',
                  application_description='thetaleapi Python library',
                  interval=1,
                  max_tries=float('inf')):
        '''Authorise application if not already authorised

        :param request_user_action_callback:
            Function used to request user action when authorisation page URL was 
            received. Must accept full URL to this page.
        :param str application_name:
            Name of the application that uses the API.
        :param str application_info:
            Short description of the device from which authorisation request 
            comes.
        :param str application_description:
            Application description (no HTML allowed).
        :param float interval:
            Interval between checks whether user has authorised the application.
        :param int max_tries:
            Maximum number of attempts before raising 
            :py:class:`thetale.request.AuthorisationTimeOutError`.

        :return:
            :py:class:`concurrent.futures.Future` instance which will eventually 
            return the same value as :py:meth:`authorisation_state` or raise 
            :py:class:`thetale.request.TheTaleAPIError`, 
            :py:class:`thetale.request.AuthorisationTimeOutError` or 
            :py:class:`thetale.request.AuthorisationRejectedError` exception.
        '''
        response = self.request(
            method='POST',
            resource_path='/accounts/third-party/tokens',
            method_name='request-authorisation',
            method_version='1.0',
            application_name=application_name,
            application_info=application_info,
            application_description=application_description,
        )
        request_user_action_callback(
            'http://the-tale.org/' + response['data']['authorisation_page'])
        return self.executor.submit(self._wait_for_authorisation, interval,
                                    max_tries)

    def _wait_for_authorisation(self, interval, max_tries):
        '''Wait until user authorises the application

        :param float interval:
            Interval between checks whether user has authorised the application.
        :param int max_tries:
            Maximum number of attempts before raising 
            :py:class:`thetale.request.AuthorisationTimeOutError`.

        :return:
            :py:class:`concurrent.futures.Future` instance which will eventually 
            return response, same as :py:meth:`_authorisation_state`, or raise 
            :py:class:`thetale.request.TheTaleAPIError`, 
            :py:class:`thetale.request.AuthorisationTimeOutError` or 
            :py:class:`thetale.request.AuthorisationRejectedError` exception.
        '''
        tries = 0
        while tries < max_tries:
            did_authorise, response = self._check_authorisation_state()
            if did_authorise:
                return response
            sleep(interval)
            tries += 1
        raise AuthorisationTimeOutError()


def find_turn(responses, turn):
    '''Find given turn in a list of response objects

    :param list responses:
        Responses to search in.
    :param int turn:
        Turn to find.

    :return:
        One of the dictionaries in ``responses`` argument. May raise 
        :py:class:`NoPatchTurn` exception.
    '''
    if turn is None:
        return None
    rs_iter = iter(responses)
    response = next(rs_iter)
    try:
        while response['data']['turn']['number'] != turn:
            response = next(rs_iter)
    except StopIteration:
        raise NoPatchTurn(
            'Previous response for turn {0} was not found'.format(turn))
    return response


class TheTaleClient(Client):
    '''Class used to communicate with the-tale.org API
    '''

    def login(self, email, password, remember):
        '''Login using given email and password

        It is highly recommended to use :py:meth:`Client.authorise`.

        :param str email:
            Login.
        :param str password:
            Password.
        :param bool remember:
            If true, user session will not expire for a long time.
        '''
        return self.request(
            method='POST',
            resource_path='/accounts/auth',
            method_name='login',
            method_version='1.0',
            email=email,
            password=password,
            **({'remember': '1'} if remember else {})
        )

    def logout(self):
        '''Logout
        '''
        return self.request(
            method='POST',
            resource_path='/accounts/auth',
            method_name='logout',
            method_version='1.0',
        )

    def info(self, account=None, prev_responses=None):
        '''Return account and hero information

        :param int account:
            Account identifier. Defaults to current account if client was logged 
            in. Must not be present if ``prev_responses`` argument is given.
        :param list prev_responses:
            List of previous response objects.

        Returns almost everything you need to know about account and associated 
        hero state.
        '''
        if not account:
            if self.account:
                account = self.account
            else:
                account = self.account = (
                    self._authorisation_state()['data']['account_id'])
        response = self.request(
            resource_path='/game',
            method_name='info',
            method_version='1.3',
            **dict(([('account', str(account))] if account else [])
                   + ([('client_turns',
                        ','.join(str(prev_response['data']['turn']['number'])
                                 for prev_response in prev_responses)),
                       ('account', prev_responses[0]['data']['account']['id'])]
                      if prev_responses else []))
        )
        if prev_responses and (response['data']['account']['hero']['patch_turn']
                               is not None):
            if response['data']['enemy']:
                patch_turn = response['data']['enemy']['hero']['patch_turn']
                prev_response = find_turn(prev_responses, patch_turn)
                if prev_response is None:
                    # Do nothing
                    pass
                elif ((not prev_response['data']['enemy']
                       and (response['data']['enemy']['hero']['patch_turn']
                            is not None))):
                    response['data']['enemy']['hero'] = self.request(
                        resource_path='/game',
                        method_name='info',
                        method_version='1.3',
                        account=str(response['data']['enemy']['id'])
                    )['data']['account']['hero']
                else:
                    prev_hero = response['data']['enemy']['hero']
                    response['data']['enemy']['hero'] = (
                        deepcopy(prev_response['data']['enemy']['hero']))
                    response['data']['enemy']['hero'].update(prev_hero)
            patch_turn = response['data']['account']['hero']['patch_turn']
            prev_response = find_turn(prev_responses, patch_turn)
            if prev_response is not None:
                prev_hero = response['data']['account']['hero']
                response['data']['account']['hero'] = (
                    deepcopy(prev_response['data']['account']['hero']))
                response['data']['account']['hero'].update(prev_hero)
        return response

    def basic_info(self):
        '''Return some generic information about game
        '''
        return self.request(
            resource_path='/',
            method_name='info',
            method_version='1.0',
        )

    def cities_info(self):
        '''Return information about cities
        '''
        return self.request(
            resource_path='/game/places',
            method_name='list',
            method_version='1.1',
        )

    def city_info(self, place):
        '''Return information about one city

        :param str place:
            Name of the city.
        '''
        return self.request(
            resource_path='/game/places/{0}'.format(place),
            method_name='show',
            method_version='2.0',
        )

    def use_ability(self, ability, *, building=None, battle=None):
        '''Use player ability

        See :py:class:`thetale.player.PlayerAbilities` for a list.
        '''
        return self.request(
            method='POST',
            resource_path='/game/abilities/{0}'.format(ability),
            method_name='use',
            method_version='1.0',
            **(
                {'building': building}
                if building else
                {'battle': battle}
                if battle else
                {}
            )
        )

    def select_quest_action(self, option_uid):
        '''Select action in quest

        :param str option_uid:
            .. todo: Create and document quest API data and reference it here.
        '''
        return self.request(
            method='POST',
            resource_path='/game/quests',
            method_name='choose',
            method_version='1.0',
            option_uid=option_uid,
        )

    def take_card(self):
        '''Take new fate card if it is available
        '''
        return self.request(
            method='POST',
            resource_path='/game/cards',
            method_name='get',
            method_version='1.0',
        )

    def join_cards(self, *cards):
        '''Join fate cards

        :param int cards:
            List of card identifiers.
        '''
        return self.request(
            method='POST',
            resource_path='/game/cards',
            method_name='combine',
            method_version='1.0',
            cards=','.join(str(card) for card in cards),
            data_in_url=True,
        )
