'''Some common utilities
'''


class NoValue:
    '''Class that represents itself as ``<NoValue>``
    '''

    @staticmethod
    def __repr__():
        return '<NoValue>'


def _get_super_repr(obj):
    '''Get representation derived from one of the base classes
    '''
    for cls in (int, dict, list, tuple):
        if isinstance(obj, cls):
            return '; ' + cls.__repr__(obj)
    return ''


class SlotRepr:
    '''Mixin that adds human-readable representation to slotted classes
    '''
    __slots__ = ()

    def __repr__(self):
        return '<' + self.__class__.__name__ + ': (' + (
            ', '.join(name + '=' + repr(getattr(self, name, None))
                      for name in getattr(self, '__slots__',
                                          getattr(self, '_slots', [])))
        ) + _get_super_repr(self) + '>'
