'''Module that contains classes describing session state
'''

from .account import AccountInfo
from .request import AuthorisationState
from .util import SlotRepr

from datetime import datetime


class SessionState(SlotRepr):
    '''Session state
    '''
    __slots__ = ('next_url', 'account_info', 'authorisation_state',
                 'expiration_time')

    def __init__(self, authorisation_state_data):
        self.next_url = authorisation_state_data['data'].get('next_url', None)
        '''String, address passed when calling method or /.

        May be ``None``.'''
        self.account_info = AccountInfo(
            account_id=authorisation_state_data['data']['account_id'],
            account_name=authorisation_state_data['data']['account_name'],
            abilities_cost_data={},
        )
        '''Authorisation information.

        See :py:class:`thetale.request.AccountInfo` for more details, 
        :py:attr:`thetale.request.AccountInfo.abilities_cost` will be empty.'''
        self.authorisation_state = AuthorisationState(
            authorisation_state_data['data']['state'])
        '''Authorisation state.

        See :py:class:`thetale.request.AuthorisationState`.'''
        self.expiration_time = datetime.fromtimestamp(
            authorisation_state_data['data']['session_expire_at'])
        '''Time when session will expire.'''
