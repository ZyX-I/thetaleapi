'''Module describing various relations from enums in other modules
'''

from .cards import FateCards, FateCardsRarity, COMPANION_FATE_CARDS
from .hero import HeroPreferences, HeroTargets, HeroPowers
from .equipment import ArtifactRarities, ArtifactEffects
from .companion import CompanionRarities

from collections import OrderedDict
from fractions import Fraction


CARDS_ADDING_ENERGY = OrderedDict((
    (FateCards.energy_drop, 10),
    (FateCards.forces_bowl, 40),
    (FateCards.magical_whirlwind, 160),
    (FateCards.energy_storm, 640),
    (FateCards.barrage_of_force, 2560),
))
'''List of cards that add energy without other side effects

Values are amounts of energy added.
'''


CARDS_ADDING_COINS = OrderedDict((
    (FateCards.handful_of_coins, 1000),
    (FateCards.weighty_purse, 4000),
    (FateCards.trunk_for_luck, 16000),
))
'''List of cards that give hero coins

Values are amounts of coins added.
'''


CARDS_MODIFYING_HONOR = OrderedDict((
    (FateCards.pride, -1600),
    (FateCards.vanity, -400),
    (FateCards.lechery, -100),
    (FateCards.gluttony, -25),
    (FateCards.moderation, 25),
    (FateCards.faithfulness, 100),
    (FateCards.modesty, 400),
    (FateCards.humility, 1600),
))
'''List of cards that modify honor trait
'''


CARDS_MODIFYING_PEACEFULNESS = OrderedDict((
    (FateCards.rage, -1600),
    (FateCards.anger, -400),
    (FateCards.greed, -100),
    (FateCards.irascibility, -25),
    (FateCards.peace, 25),
    (FateCards.friendliness, 100),
    (FateCards.discretion, 400),
    (FateCards.peacefulness, 1600),
))
'''List of cards that modify peacefulness trait
'''

CARDS_RESETTING_HERO_PREFERENCES = {
    FateCards.enemy_acquirement: {HeroPreferences.favourite_prey},
    FateCards.new_homeland: {HeroPreferences.home_city},
    FateCards.new_teammate: {HeroPreferences.risk_level},
    FateCards.new_opponent: {HeroPreferences.opponent},
    FateCards.enlightenment: {HeroPreferences.religiousness},
    FateCards.tastes_in_gear: {HeroPreferences.equipment},
    FateCards.definition_of_bravery: {HeroPreferences.risk_level},
    FateCards.wearied_property: {HeroPreferences.favourite_gear},
    FateCards.combat_style_revision: {HeroPreferences.archetype},
    FateCards.revision_of_valuables: set(HeroPreferences),
}
'''List of cards that reset hero preferences change lag

Maps cards to the set of preferences that are being affected.
'''


CARDS_MODIFYING_HERO_TARGET = {
    FateCards.strange_itching: HeroTargets.healing,
    FateCards.store_push: HeroTargets.shopping,
    FateCards.aspiration_for_perfection: HeroTargets.sharpening,
    FateCards.thirst_for_knowledge: HeroTargets.educating,
    FateCards.concern_about_property: HeroTargets.fixing,
}
'''List of cards that set hero target to a different one

Values are new targets.
'''


CARDS_ADDING_EXPERIENCE = OrderedDict((
    (FateCards.clever_idea, 25),
    (FateCards.pure_reason, 100),
    (FateCards.unexpected_complications, 400),
    (FateCards.word_of_gzanzar, 1600),
    (FateCards.gzanzar_blessing, 6400),
))
'''List of cards that give hero more experience

Values are amounts of experience given.
'''


CARDS_ADDING_INFLUENCE = OrderedDict((
    (FateCards.new_facts, 20),
    (FateCards.special_operation, 80),
    (FateCards.word_of_dabnglan, 320),
    (FateCards.dabnglan_blessing, 1280),
    (FateCards.ace_in_the_hole, 5120),
))
'''List of cards that make current quest generate more influence

Values are amounts of influence generated.
'''


CARDS_DECREASING_INFLUENCE = OrderedDict((
    (FateCards.grimace_of_fortune, 10),
    (FateCards.nasty_day, 40),
    (FateCards.unexpected_trouble, 160),
    (FateCards.disastrous_event, 640),
    (FateCards.black_stripe, 2560)
))
'''List of cards that make given master have less influence

Values are amounts of influence.
'''


_inf_bonuses = (10, 40, 160, 640, 2560)


CARDS_INCREASING_CITY_POSITIVE_INFLUENCE_BONUS = OrderedDict((
    (FateCards.lucrative_contract, _inf_bonuses[0]),
    (FateCards.lovely_days, _inf_bonuses[1]),
    (FateCards.business_day, _inf_bonuses[2]),
    (FateCards.city_holiday, _inf_bonuses[3]),
    (FateCards.economic_growth, _inf_bonuses[4]),
))
'''List of cards that increase city positive influence bonus

Values are amounts of bonus added (in parts of 1, not in per cents).
'''


CARDS_INCREASING_CITY_NEGATIVE_INFLUENCE_BONUS = OrderedDict((
    (FateCards.broken_contract, _inf_bonuses[0]),
    (FateCards.nasty_weather, _inf_bonuses[1]),
    (FateCards.desolation, _inf_bonuses[2]),
    (FateCards.plague_of_rats, _inf_bonuses[3]),
    (FateCards.economic_recession, _inf_bonuses[4]),
))
'''List of cards that increase city negative influence bonus

Values are amounts of bonus added (in parts of 1, not in per cents).
'''


CARDS_INCREASING_MASTER_POSITIVE_INFLUENCE_BONUS = OrderedDict((
    (FateCards.smile_of_fortune, _inf_bonuses[0]),
    (FateCards.good_day, _inf_bonuses[1]),
    (FateCards.unexpected_benefit, _inf_bonuses[2]),
    (FateCards.good_scam, _inf_bonuses[3]),
    (FateCards.crime_of_century, _inf_bonuses[4]),
))
'''List of cards that increase master positive influence bonus

Values are amounts of bonus added (in parts of 1, not in per cents).
'''


CARDS_CREATING_HELP_RECORDS = OrderedDict((
    (FateCards.error_in_archives, 1),
    (FateCards.fake_recommendations, 4),
    (FateCards.feast_on_board, 16),
    (FateCards.plots, 64),
))
'''List of cards that create fake help records

Values are amounts of records added.
'''


CARDS_EXCHANGING_EXPERIENCE_FOR_ENERGY = OrderedDict((
    # pylint: disable=abstract-class-instantiated
    (FateCards.amnesia, Fraction(1, 6)),
    (FateCards.energy_donor, Fraction(1, 4)),
    (FateCards.debt_collection, Fraction(1, 3)),
    (FateCards.energy_ritual, Fraction(1, 2)),
))
'''List of cards that exchange experience for energy

Values are amounts of energy per one experience point. All are 
:py:class:`fractions.Fraction` instances.
'''


CARDS_GIVING_ARTIFACTS = OrderedDict((
    (FateCards.useful_gift, ArtifactRarities.common),
    (FateCards.rare_acquisition, ArtifactRarities.rare),
    (FateCards.custodian_gift, ArtifactRarities.epic),
))
'''Lists of cards that give hero useful artifacts

Values are artifact rarities.
'''


CARDS_CREATING_CUSTODIAN_GIFTS = OrderedDict((
    (FateCards.magic_coin, 20),
    (FateCards.magic_pot, 80),
    (FateCards.magic_tablecloth, 320),
    (FateCards.illimitable_wealth, 1280),
    (FateCards.cornucopia, 5120),
))
'''List of cards that create custodian gifts in some city

Values are amounts of gifts created.
'''


CARDS_GIVING_COMPANIONS = OrderedDict((
    (FateCards.common_companion, CompanionRarities.common),
    (FateCards.uncommon_companion, CompanionRarities.uncommon),
    (FateCards.rare_companion, CompanionRarities.rare),
    (FateCards.epic_companion, CompanionRarities.epic),
    (FateCards.legendary_companion, CompanionRarities.legendary)
))
'''List of cards that give companions

Values are companion rarities.
'''


assert set(CARDS_GIVING_COMPANIONS) == COMPANION_FATE_CARDS, (
    'Companion cards not found in one or the other set: {0!r}'.format(
        set(CARDS_GIVING_COMPANIONS) ^ COMPANION_FATE_CARDS))


CARDS_HEALING_COMPANIONS = OrderedDict((
    (FateCards.respite, 10),
    (FateCards.plantain, 40),
    (FateCards.sacred_honey, 160),
    (FateCards.rejuvenating_apple, 640),
    (FateCards.water_of_life, 2560),
))
'''List of cards that heal companions

Values are amounts of health healed.
'''


CARDS_COMMON = {
    FateCards.sudden_discovery,
    FateCards.handful_of_coins,
    FateCards.hand_of_death,
    FateCards.strange_itching,
    FateCards.store_push,
    FateCards.aspiration_for_perfection,
    FateCards.thirst_for_knowledge,
    FateCards.concern_about_property,
    FateCards.caring,
    FateCards.energy_drop,
    FateCards.magic_coin,
    FateCards.new_facts,
    FateCards.clever_idea,
    FateCards.common_companion,
    FateCards.four_winds,
    FateCards.respite,
    FateCards.smile_of_fortune,
    FateCards.lucrative_contract,
    FateCards.broken_contract,
    FateCards.grimace_of_fortune,
}
'''Set of cards with common rarity'''


CARDS_UNCOMMON = {
    FateCards.choice,
    FateCards.amnesia,
    FateCards.tastes_in_gear,
    FateCards.magical_grindstone,
    FateCards.magic_pot,
    FateCards.irascibility,
    FateCards.other_concerns,
    FateCards.enemy_acquirement,
    FateCards.wearied_property,
    FateCards.unexpected_complications,
    FateCards.new_homeland,
    FateCards.new_opponent,
    FateCards.new_teammate,
    FateCards.definition_of_bravery,
    FateCards.new_vision,
    FateCards.sensitivity,
    FateCards.error_in_archives,
    FateCards.combat_style_revision,
    FateCards.lovely_days,
    FateCards.useful_gift,
    FateCards.enlightenment,
    FateCards.special_operation,
    FateCards.peace,
    FateCards.teleport,
    FateCards.weighty_purse,
    FateCards.good_day,
    FateCards.nasty_weather,
    FateCards.moderation,
    FateCards.fairy_fixer,
    FateCards.forces_bowl,
    FateCards.gluttony,
    FateCards.uncommon_companion,
    FateCards.new_doors,
    FateCards.plantain,
    FateCards.pure_reason,
    FateCards.nasty_day,
}
'''Set of cards with uncommon rarity'''


CARDS_RARE = {
    FateCards.tardis,
    FateCards.greed,
    FateCards.lechery,
    FateCards.faithfulness,
    FateCards.energy_donor,
    FateCards.friendliness,
    FateCards.desolation,
    FateCards.magical_whirlwind,
    FateCards.unexpected_benefit,
    FateCards.revision_of_valuables,
    FateCards.rare_acquisition,
    FateCards.magic_tablecloth,
    FateCards.word_of_gzanzar,
    FateCards.word_of_dabnglan,
    FateCards.trunk_for_luck,
    FateCards.business_day,
    FateCards.fake_recommendations,
    FateCards.rare_companion,
    FateCards.sacred_honey,
    FateCards.unexpected_trouble,
}
'''Set of cards with rare rarity'''


CARDS_EPIC = {
    FateCards.great_creator_blessing,
    FateCards.gzanzar_blessing,
    FateCards.dabnglan_blessing,
    FateCards.debt_collection,
    FateCards.magic_tool,
    FateCards.anger,
    FateCards.city_holiday,
    FateCards.custodian_gift,
    FateCards.feast_on_board,
    FateCards.plague_of_rats,
    FateCards.illimitable_wealth,
    FateCards.discretion,
    FateCards.modesty,
    FateCards.essence_of_things,
    FateCards.vanity,
    FateCards.good_scam,
    FateCards.energy_storm,
    FateCards.epic_companion,
    FateCards.rejuvenating_apple,
    FateCards.hidden_potential,
    FateCards.disastrous_event,
}
'''Set of cards with epic rarity'''


CARDS_LEGENDARY = {
    FateCards.pride,
    FateCards.plots,
    FateCards.peacefulness,
    FateCards.inspiration,
    FateCards.crime_of_century,
    FateCards.energy_ritual,
    FateCards.cornucopia,
    FateCards.humility,
    FateCards.barrage_of_force,
    FateCards.economic_growth,
    FateCards.economic_recession,
    FateCards.rage,
    FateCards.legendary_companion,
    FateCards.water_of_life,
    FateCards.ace_in_the_hole,
    FateCards.black_stripe,
}
'''Set of cards with legendary rarity'''


RARITY_SETS = OrderedDict((
    (FateCardsRarity.common, CARDS_COMMON),
    (FateCardsRarity.uncommon, CARDS_UNCOMMON),
    (FateCardsRarity.rare, CARDS_RARE),
    (FateCardsRarity.epic, CARDS_EPIC),
    (FateCardsRarity.legendary, CARDS_LEGENDARY),
))
'''List of cards rarity

Value are sets of cards with this rarity.
'''


CARDS_RARITY = {}
'''Dictionary mapping cards to their rarity'''


for rarity, cards in RARITY_SETS.items():
    for card in cards:
        CARDS_RARITY[card] = rarity
        del card
    del rarity
    del cards


assert set(CARDS_RARITY) == set(FateCards), 'Cards not found: {0!r}'.format(
    set(FateCards) - set(CARDS_RARITY))


EFFECTS_ADDING_POWERS = {
    ArtifactEffects.astral_barrier: HeroPowers.gargoyle,
    ArtifactEffects.vampirism: HeroPowers.vampire_blow,
    ArtifactEffects.ice: HeroPowers.frost,
    ArtifactEffects.flame: HeroPowers.fireball,
    ArtifactEffects.last_cast: HeroPowers.last_cast,
    ArtifactEffects.regeneration: HeroPowers.regeneration,
    ArtifactEffects.accurate_attacks: HeroPowers.critical_hit,
    ArtifactEffects.acceleration: HeroPowers.acceleration,
    ArtifactEffects.poison: HeroPowers.poisonous_cloud,
}
'''Dictionary mapping some of the artifact effects to the hero powers they add
'''
