'''Module that contains classes describing hero companion
'''

from .util import SlotRepr

from enum import Enum


class CompanionTypes(Enum):
    '''Possible companion types
    '''
    alive = 1
    '''Alive companion.'''
    mesomechanic = 2
    '''Mesomechanic companion.'''
    uncommon = 3
    '''Uncommon companion.'''


class CompanionRarities(Enum):
    '''Possible companion rarities
    '''
    common = 0
    uncommon = 1
    rare = 2
    epic = 3
    legendary = 4



class CompanionState(SlotRepr):
    '''Companion description

    Two companions are considered equal if their identifier, name, health, 
    max_health, experience and real coherence is equal.
    '''
    __slots__ = ('id', 'name', 'health', 'max_health', 'experience',
                 'level_experience', 'effective_coherence', 'real_coherence')

    def __init__(self, companion_data):
        self.id = companion_data['type']
        '''Companion identifier.

        Some integer. It is not safe to assume anything else about it.'''
        self.name = companion_data['name']
        '''Companion name.'''
        self.health = companion_data['health']
        '''Amount of health the companion currently has, integer.'''
        self.max_health = companion_data['max_health']
        '''Maximum amount of health the companion can have, integer.'''
        self.experience = companion_data['experience']
        '''Current companion experience with this hero, integer.'''
        self.level_experience = companion_data['experience_to_level']
        '''Experience companion must gain for the new level promotion, integer.
        '''
        self.effective_coherence = companion_data['coherence']
        '''Current level of coherence, integer.

        Provides only effective value which is capped according to hero powers.
        '''
        self.real_coherence = companion_data['real_coherence']
        '''Current level of coherence, integer, not capped.

        Provides current coherence level, not capped according to hero powers.
        '''

    def __eq__(self, other):
        return (
            self.id == other.id
            and self.name == other.name
            and self.health == other.health
            and self.max_health == other.max_health
            and self.experience == other.experience
            and self.real_coherence == other.real_coherence
        )
