'''Module that contains classes describing player hero
'''

from .util import SlotRepr
from .equipment import EquipmentState, BagState
from .messages import Messages
from .action import Action
from .companion import CompanionState

from enum import Enum


class CombatPowerType(Enum):
    '''Possible types regarding battles
    '''
    active = ()
    passive = ()
    non_combat = ()


class PowerTypes(Enum):
    '''Possible power types

    Values in tuples:
    #. True if power is suitable for monsters.
    #. True if power is suitable for heroes.
    #. Combat type.
    '''
    non_combat = (False, True, CombatPowerType.non_combat)
    monster_passive = (True, False, CombatPowerType.passive)
    passive = (True, True, CombatPowerType.passive)
    active = (True, True, CombatPowerType.active)


class HeroPreferences(Enum):
    '''Possible hero preferences
    '''
    religiousness = 'религиозность'
    home_city = 'родной город'
    risk_level = 'уровень риска'
    teammate = 'соратник'
    favourite_gear = 'любимая вещь'
    opponent = 'противник'
    archetype = 'архетип'
    equipment = 'экипировка'
    favourite_prey = 'любимая добыча'


class HeroTargets(Enum):
    '''Possible targets for which money is collected
    '''
    shopping = 'покупка артефакта'
    fixing = 'починка артефакта'
    healing = 'лечение'
    sharpening = 'заточка артефакта'
    educating = 'обучение'


class HeroRaces(Enum):
    '''Possible races
    '''
    human = 0
    elf = 1
    ork = 2
    goblin = 3
    dwarf = 4


class HeroGenders(Enum):
    '''Possible hero genders
    '''
    male = 0
    female = 1
    other = 2


class HeroPowers(Enum):
    '''Possible hero powers
    '''
    def __init__(self, name, typ):
        suitable_for_monsters, suitable_for_heroes, combat_type = typ.value
        self.is_monsters = suitable_for_monsters
        self.is_heroes = suitable_for_heroes
        self.combat_type = combat_type
        self.active = combat_type is CombatPowerType.active
        self.passive = combat_type is CombatPowerType.passive
        self.combat = combat_type is not CombatPowerType.non_combat
        self.power_name = name

    berserk = ('Берсерк', PowerTypes.passive)
    fighter = ('Боец', PowerTypes.passive)
    vagrant = ('Бродяга', PowerTypes.non_combat)
    faster_then_wind = ('Быстрее ветра', PowerTypes.monster_passive)
    fast = ('Быстрый', PowerTypes.passive)
    warrior = ('Воин', PowerTypes.passive)
    magical_mushroom = ('Волшебный гриб', PowerTypes.active)
    gargoyle = ('Горгулья', PowerTypes.passive)
    burglar = ('Громила', PowerTypes.passive)
    businessman = ('Делец', PowerTypes.non_combat)
    diplomatic = ('Дипломатичный', PowerTypes.non_combat)
    frost = ('Заморозка', PowerTypes.active)
    thrifty = ('Запасливый', PowerTypes.non_combat)
    healthy = ('Здоровяк', PowerTypes.passive)
    bag_of_bones = ('Кожа да кости', PowerTypes.monster_passive)
    critical_hit = ('Критический удар', PowerTypes.passive)
    mage = ('Маг', PowerTypes.passive)
    slow = ('Медленный', PowerTypes.monster_passive)
    clumsy = ('Неповоротливый', PowerTypes.monster_passive)
    ninja = ('Ниндзя', PowerTypes.passive)
    gifted = ('Одарённый', PowerTypes.non_combat)
    last_cast = ('Последний шанс', PowerTypes.monster_passive)
    captious = ('Придирчивый', PowerTypes.non_combat)
    start_push = ('Разбег-толчок', PowerTypes.active)
    regeneration = ('Регенерация', PowerTypes.active)
    weak = ('Слабый', PowerTypes.monster_passive)
    fat = ('Толстяк', PowerTypes.monster_passive)
    tradesman = ('Торгаш', PowerTypes.non_combat)
    reed = ('Тростинка', PowerTypes.monster_passive)
    heavy_blow = ('Тяжёлый удар', PowerTypes.active)
    killer = ('Убийца', PowerTypes.passive)
    blow = ('Удар', PowerTypes.active)
    vampire_blow = ('Удар вампира', PowerTypes.active)
    acceleration = ('Ускорение', PowerTypes.active)
    charismatic = ('Харизматичный', PowerTypes.non_combat)
    thin = ('Худой', PowerTypes.monster_passive)
    step_aside = ('Шаг в сторону', PowerTypes.active)
    fireball = ('Шар огня', PowerTypes.active)
    dandy = ('Щёголь', PowerTypes.non_combat)
    essential_magnet = ('Эфирный магнит', PowerTypes.non_combat)
    poisonous_cloud = ('Ядовитое облако', PowerTypes.active)


class HeroPvPState(SlotRepr):
    '''Hero PvP state
    '''
    __slots__ = ('advantage', 'effectiveness', 'energy', 'effectiveness_bonus',
                 'ice_probability', 'blood_probability', 'flame_probability',
                 'energy_regeneration_rate')

    def __init__(self, hero_pvp_data, effectiveness_bonus):
        self.advantage = hero_pvp_data['advantage']
        '''Hero advantage, integer.'''
        self.effectiveness = hero_pvp_data['effectiveness']
        '''Hero effectiveness, integer.'''
        self.energy = hero_pvp_data['energy']
        '''Available energy, integer.'''
        self.ice_probability = hero_pvp_data['probabilities']['ice']
        '''Probability of successfull ice usage, floating point.'''
        self.blood_probability = hero_pvp_data['probabilities']['blood']
        '''Probability of successfull blood usage, floating point.'''
        self.flame_probability = hero_pvp_data['probabilities']['flame']
        '''Probability of successfull flame usage, floating point.'''
        self.effectiveness_bonus = effectiveness_bonus
        '''PvP effectiveness bonus that is a derivative of player might.'''
        self.energy_regeneration_rate = hero_pvp_data['energy_speed']
        '''How fast energy is regenerating, strictly positive integer.'''


class TraitStateEnum(Enum):
    '''Enum with 3-tuple values

    Values should be tuples (state name, minimal value, maximal value).
    '''
    def __init__(self, names, minimal, maximal):
        self.state_names = names
        '''Possible names of the trait state.'''
        self.minimal_value = minimal
        '''Minimal value required for this state to be in effect.

        For state to be in effect it must be inside [minimal_value, 
        maximal_value] closed (inclusive) interval.'''
        self.maximal_value = maximal
        '''Maximal value required for this state to be in effect.

        For state to be in effect it must be inside [minimal_value, 
        maximal_value] closed (inclusive) interval.'''

    @classmethod
    def from_name(cls, name):
        '''Generate class instance from state name
        '''
        for i in cls:
            if name in i.state_names:
                return i
        raise KeyError(name)

    @classmethod
    def from_value(cls, value):
        '''Generate class instance from value
        '''
        for i in cls:
            if i.minimal_value <= value <= i.maximal_value:
                return i
        raise IndexError('Required value was not found')

    def __ge__(self, other):
        if self.__class__ is other.__class__:
            return self.minimal_value >= other.minimal_value
        return NotImplemented

    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self.minimal_value > other.minimal_value
        return NotImplemented

    def __le__(self, other):
        if self.__class__ is other.__class__:
            return self.minimal_value <= other.minimal_value
        return NotImplemented

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.minimal_value < other.minimal_value
        return NotImplemented


class HeroHonorStates(TraitStateEnum):
    '''Possible honor values (quality-wise)

    Values are tuples (state name, minimal value, maximal value).
    '''
    dishonest = (('бесчестный', 'бесчестная', 'бесчестное'), -1000, -700)
    mean = (('подлый', 'подлая', 'подлое'), -699, -300)
    defective = (('порочный', 'порочная', 'порочное'), -299, -100)
    canny = (('себе на уме',), -99, 99)
    decent = (('порядочный', 'порядочная', 'порядочное'), 100, 299)
    noble = (('благородный', 'благородная', 'благородное'), 300, 699)
    man_of_his_word = (('хозяин своего слова', 'хозяйка своего слова'), 700, 1000)


class HeroPeacefulnessStates(TraitStateEnum):
    '''Possible peacefulness values (quality-wise)

    Values are tuples (state name, minimal value, maximal value).
    '''
    near_to_massacre = (('скорый на расправу', 'скорая на расправу',
                         'скорое на расправу'), -1000, -700)
    hot_tempered = (('вспыльчивый', 'вспыльчивая', 'вспыльчивое'), -699, -300)
    roughneck = (('задира',), -299, -100)
    discreet = (('сдержанный', 'сдержанная', 'сдержанное'), -99, 99)
    well_wisher = (('доброхот',), 100, 299)
    pacific = (('миролюбивый', 'миролюбивая', 'миролюбивое'), 300, 699)
    humanist = (('гуманист',), 700, 1000)


class HeroPosition(SlotRepr):
    '''Hero position and direction

    Don’t ask me in which units directions are expressed.
    '''
    __slots__ = ('x', 'y', 'dx', 'dy')

    def __init__(self, position_data):
        self.x = float(position_data['x'])
        '''X coordinate, floating-point.'''
        self.y = float(position_data['y'])
        '''X coordinate, floating-point.'''
        self.dx = float(position_data['dx'])
        '''X direction, floating-point.'''
        self.dy = float(position_data['dy'])
        '''Y direction, floating-point.'''


class HeroTraitsState(SlotRepr):
    '''State of hero traits: honor and peacefulness
    '''
    __slots__ = ('honor', 'peacefulness', 'honor_state', 'peacefulness_state',
                 'honor_state_name', 'peacefulness_state_name')

    def __init__(self, habits_data):
        self.honor = habits_data['honor']['raw']
        '''Current value of the honor trait, floating-point.'''
        self.peacefulness = habits_data['peacefulness']['raw']
        '''Current value of the peacefulness trait, floating-point.'''
        self.honor_state = HeroHonorStates.from_value(
            habits_data['honor']['raw'])
        '''Current effective (quality-wise) value of the honor trait.'''
        self.peacefulness_state = HeroPeacefulnessStates.from_value(
            habits_data['peacefulness']['raw'])
        '''Current effective (quality-wise) value of the peacefulness trait.'''
        self.honor_state_name = habits_data['honor']['verbose']
        '''Current used name of the effective honor trait value.'''
        self.peacefulness_state_name = habits_data['peacefulness']['verbose']
        '''Current used name of the effective peacefulness trait value.'''


class HeroState(SlotRepr):
    '''Hero description

    Two heroes are considered equal if their accounts are equal.
    '''
    __slots__ = ('account', 'in_pvp_queue', 'pvp_state', 'equipment', 'bag',
                 'experience', 'race', 'health', 'name', 'level', 'gender',
                 'level_experience', 'max_health', 'powers_points', 'money',
                 'alive', 'physical_power', 'magical_power', 'moving_speed',
                 'initiative', 'traits', 'log', 'diary', 'action', 'companion',
                 'position')

    def __init__(self, game, account, hero_data, in_pvp_queue):
        # pylint: disable=too-many-statements
        self.account = account
        '''Account with which hero is associated.'''
        self.in_pvp_queue = in_pvp_queue
        '''True if hero is in pvp queue.'''
        if hero_data['pvp']:
            self.pvp_state = HeroPvPState(hero_data['pvp'], account.pvp_effectiveness_bonus)
            '''PvP state if game is in PvP mode, otherwise ``None``.'''
        else:
            self.pvp_state = None
        self.equipment = EquipmentState(hero_data['equipment'])
        '''Currently present equipment.'''
        self.bag = BagState(hero_data['bag'], hero_data['secondary']['max_bag_size'])
        '''Items present in bag.'''
        base_data = hero_data['base']
        self.level = base_data['level']
        '''Hero level, integer.'''
        self.experience = base_data['experience']
        '''Experience hero currently has, integer.'''
        self.level_experience = base_data['experience_to_level']
        '''Experience hero must gain for the new level promotion, integer.'''
        self.race = HeroRaces(base_data['race'])
        '''Hero race, see :py:class:`HeroRaces`.'''
        self.gender = HeroGenders(base_data['gender'])
        '''Hero gender, see :py:class:`HeroGenders`.'''
        self.health = base_data['health']
        '''Amount of health the hero currently has, integer.'''
        self.max_health = base_data['max_health']
        '''Amount of health the hero will have if completely healed, integer.'''
        self.name = base_data['name']
        '''Hero name, string.'''
        self.powers_points = base_data['destiny_points']
        '''Number of powers that may be additionally chosen for the hero.'''
        self.money = base_data['money']
        '''Amount of money hero currently has, integer.'''
        self.alive = base_data['alive']
        '''True if hero is alive, false otherwise.'''
        secondary_data = hero_data['secondary']
        self.physical_power = secondary_data['power'][0]
        '''Amount of physical power hero currently has, integer.'''
        self.magical_power = secondary_data['power'][1]
        '''Amount of magical power hero currently has, integer.'''
        self.moving_speed = secondary_data['move_speed']
        '''Hero moving speed, floating-point.'''
        self.initiative = secondary_data['initiative']
        '''Hero initiative, floating-point.'''
        self.traits = HeroTraitsState(hero_data['habits'])
        '''Hero traits: peacefulness and honor.'''
        self.log = Messages(game.turn, hero_data['messages'])
        '''Current log messages.'''
        self.diary = Messages(game.turn, hero_data['diary'])
        '''Current diary messages.'''
        self.action = Action(hero_data['action'])
        '''Current hero action.'''
        if hero_data['companion']:
            self.companion = CompanionState(hero_data['companion'])
            '''Companion state if companion is present or ``None``.'''
        else:
            self.companion = None
        self.position = HeroPosition(hero_data['position'])
        # TODO: quests

    def __eq__(self, other):
        return self.account == other.account
