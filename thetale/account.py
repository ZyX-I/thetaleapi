'''Module that contains classes describing player account
'''

from .hero import HeroState
from .player import PlayerAbilities
from .cards import FateCardsState
from .util import SlotRepr

from datetime import datetime


class EnergyInfo(int, SlotRepr):
    '''Information about available and maximum energy and its future usage
    '''
    _slots = ('total', 'bonus', 'regular', 'discount', 'maximum')

    def __new__(cls, energy_data):
        total = energy_data['bonus'] + energy_data['value']
        return int.__new__(cls, total)

    def __init__(self, energy_data):
        super(EnergyInfo, self).__init__()
        self.total = int(self)
        '''Amount of energy available for use.'''
        self.bonus = energy_data['bonus']
        '''Amount of bonus energy available.'''
        self.regular = energy_data['value']
        '''Amount of non-bonus energy available.'''
        self.discount = energy_data['discount']
        '''Energy discount value (the result of artifact(s)’ effects).'''
        self.maximum = energy_data['max']
        '''Maximum amount of non-bonus energy possible.'''


class PermissionsState(SlotRepr):
    '''Current user permissions for the given account
    '''
    __slots__ = ('building_repair', 'pvp')

    def __init__(self, permissions_data):
        self.building_repair = permissions_data['can_repair_building']
        '''True if user can order to repair building.

        True if current user can his account’s energy to repair building.
        '''
        self.pvp = permissions_data['can_participate_in_pvp']
        '''True if user can order to move hero to PvP queue.
        '''


class AccountState(SlotRepr):
    '''Account description

    Accounts are considered equal if their identifiers are equal.
    '''
    __slots__ = ('id', 'is_own', 'is_old', 'hero', 'last_visit', 'might',
                 'pvp_effectiveness_bonus', 'help_critical_chance', 'cards',
                 'messages', 'energy', 'permissions')

    def __init__(self, game, account_data):
        self.id = account_data['id']
        '''Unique account identifier.

        Some integer. It is not safe to assume anything else about it, except 
        that two equal accounts have equal id.
        '''
        self.is_own = account_data['is_own']
        '''True if the instance describes own account.'''
        self.is_old = account_data['is_old']
        '''True if the instance contains outdated information.'''
        self.last_visit = datetime.fromtimestamp(account_data['last_visit'])
        '''Last visit date and time.'''
        self.might = account_data['hero']['might']['value']
        '''Player might, float.'''
        self.pvp_effectiveness_bonus = (
            account_data['hero']['might']['pvp_effectiveness_bonus'])
        '''PvP effectiveness bonus (might derivative), float.'''
        self.help_critical_chance = account_data['hero']['might']['crit_chance']
        '''Help critical change (might derivative), float.'''
        self.cards = FateCardsState(account_data['hero']['cards'])
        '''Information about currently present fate cards.'''
        self.energy = EnergyInfo(account_data['hero']['energy'])
        '''Information about available energy.'''
        self.hero = HeroState(game, self, account_data['hero'],
                              account_data['in_pvp_queue'])
        '''Hero state.'''
        self.permissions = PermissionsState(account_data['hero']['permissions'])
        '''Current user permissions.'''

    def __eq__(self, other):
        return self.id == other.id


class AccountInfo(SlotRepr):
    '''Incomplete account information
    '''
    __slots__ = ('id', 'name', 'abilities_cost')

    def __init__(self, account_id, account_name, abilities_cost_data):
        self.id = account_id
        '''Unique account identifier (if logged in) or ``None``.

        Some integer. It is not safe to assume anything else about it, except 
        that two equal accounts have equal id.
        '''
        self.name = account_name
        '''Player name (if logged in) or ``None``.'''
        self.abilities_cost = {}
        '''Dictionary mapping player abilities to their energy cost (int).

        May be empty.

        Check :py:class:`thetale.player.PlayerAbilities` for possible 
        abilities.'''
        for k, v in abilities_cost_data.items():
            self.abilities_cost[PlayerAbilities[k]] = v

    def __eq__(self, other):
        return self.id == other.id
