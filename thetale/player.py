'''Module that contains classes describing player
'''

from enum import Enum


class PlayerAbilities(Enum):
    '''List of player abilities
    '''
    help = 'help'
    '''Help your hero'''
    arena_pvp_1x1 = 'arena_pvp_1x1'
    '''Send your hero to the PvP arena'''
    arena_pvp_1x1_leave_queue = 'arena_pvp_1x1_queue'
    '''Withdraw your hero from PvP arena queue'''
    arena_pvp_1x1_accept = 'arena_pvp_1x1_accept'
    '''Accept challenge in PvP arena list'''
    building_repair = 'building_repair'
    '''Repair building'''
    drop_item = 'drop_item'
    '''Drop item from hero inventory'''
