'''Entry point to the API client
'''

from .request import TheTaleClient
from .info import Info, BasicInfo
from .city import CitiesInfo, CityInfo
from .session import SessionState


class Game(TheTaleClient):
    '''Class that does all the job talking with the-tale.org API
    '''
    def info(self, account=None, prev_infos=None):
        '''Get information about the current game state

        :param int | thetale.account.AccountState account:
            Account identifier. Defaults to current account if client was logged 
            in. Must not be present if ``prev_info`` argument is given.
        :param list prev_infos:
            List of previous info objects.

        :return: :py:class:`thetale.info.Info` instance.
        '''
        return Info(super(Game, self).info(
            account=((account if isinstance(account, int) else account.id)
                     if account is not None else None),
            prev_responses=([prev_info.response for prev_info in prev_infos]
                            if prev_infos else None),
        ))

    def basic_info(self):
        '''Get some basic information about game

        :return: :py:class:`thetale.game.GameInfo`
        '''
        return BasicInfo(super(Game, self).basic_info())

    def cities_info(self):
        '''Get a list of cities with basic information about them

        :return: :py:class:`thetale.city.CitiesInfo`
        '''
        return CitiesInfo(super(Game, self).cities_info())

    def city_info(self, city):
        '''Get an information about one city

        :param thetale.city.CityBasicInfo city:
            City to get detailed information about.

        :return: :py:class:`thetale.city.CityInfo`
        '''
        return CityInfo(super(Game, self).city_info(city.id))

    def use_ability(self, ability, **kwargs):
        '''Use player ability

        See :py:class:`thetale.player.PlayerAbilities` for a list.
        '''
        return super(Game, self).use_ability(ability.value, **kwargs)

    def join_cards(self, *cards):
        '''Join fate cards

        :param thetale.cards.FateCardState cards:
            List of cards that need to be joined.
        '''
        return super(Game, self).join_cards(*[card.id for card in cards])

    def _wait_for_authorisation(self, *args, **kwargs):
        '''Wait until user authorises the application

        Wraps :py:meth:`thetale.request.Client._wait_for_authorisation` to 
        return the same thing as :py:meth:`authorisation_state`.

        :return: :py:class:`thetale.session.SessionState`
        '''
        return SessionState(super(Game, self)._wait_for_authorisation(
            *args, **kwargs))

    def authorisation_state(self):
        '''Authorisation state

        :return: :py:class:`thetale.session.SessionState`
        '''
        return SessionState(super(Game, self).authorisation_state())
