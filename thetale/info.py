'''Top level all-inclusive game information
'''

from .util import SlotRepr
from .game import GameState, GameVersionInfo
from .account import AccountState, AccountInfo


class Info(SlotRepr):
    '''Class providing access to top-level all-inclusive game information

    This is the one that is returned by 
    :py:meth:`thetale.request.TheTaleClient.info` method.
    '''
    __slots__ = ('game', 'account', 'enemy_account', 'response')

    def __init__(self, response):
        self.game = GameState(response['data'])
        '''Generic game information, see :py:class:`thetale.game.Game`.'''
        if response['data']['account']:
            self.account = AccountState(self.game, response['data']['account'])
            '''All available account information, if any.

            May be ``None`` if account was not specified.
            '''
        else:
            self.account = None
        if response['data']['enemy']:
            self.enemy_account = AccountState(self.game,
                                              response['data']['enemy'])
            '''All available enemy account information, if any.

            May be ``None`` if there is no PvP
            '''
        else:
            self.enemy_account = None
        self.response = response
        '''Original response: parsed JSON object.

        Use of this attribute is highly discouraged. Use it only if data is not 
        accessible by other means.
        '''

    @property
    def hero(self):
        '''Shortcut to the hero state'''
        return self.account.hero

    @property
    def energy(self):
        '''Shortcut to the currently available energy'''
        return self.account.energy

    @property
    def cards(self):
        '''Shortcut to the information about currently present fate cards'''
        return self.account.cards

    @property
    def equipment(self):
        '''Shortcut to the hero equipment state'''
        return self.account.hero.equipment

    @property
    def bag(self):
        '''Shortcut to the hero bag state'''
        return self.account.hero.bag

    @property
    def companion(self):
        '''Shortcut to the hero companion state'''
        return self.account.hero.companion


class BasicInfo(SlotRepr):
    '''Basic information about game
    '''
    __slots__ = ('dynamic_content_url', 'static_content_url', 'game_version',
                 'game_version_info', 'turn_length', 'account_info', 'response')

    def __init__(self, response):
        game_data = response['data']
        self.dynamic_content_url = game_data['dynamic_content']
        '''Base absolute path to dynamic game content (e.g. map).'''
        self.static_content_url = game_data['static_content']
        '''Base absolute path to static game content (e.g. images).'''
        self.game_version = game_data['game_version']
        '''Game version, string.'''
        self.game_version_info = GameVersionInfo(game_data['game_version'])
        '''Game version info, tuple similar to :py:attribute:`sys.version_info`.
        '''
        self.turn_length = game_data['turn_delta']
        '''Delay between two turns, in seconds, integer.'''
        self.account_info = AccountInfo(
            game_data['account_id'],
            game_data['account_name'],
            game_data['abilities_cost'],
        )
        '''Account information, see :py:class:`thetale.account.AccountInfo`.'''
        self.response = response
        '''Original response: parsed JSON object

        Use of this attribute is highly discouraged. Use it only if data is not 
        accessible by other means.
        '''

    @property
    def abilities_cost(self):
        '''Shortcut for abilities cost for the current account'''
        return self.account_info.abilities_cost
