'''Module that contains classes describing hero equipment
'''

from .util import SlotRepr

from enum import Enum


class EquipmentSlots(Enum):
    '''Possible slots occupied by equipment
    '''
    primary_hand = '0'
    secondary_hand = '1'
    helmet = '2'
    amulet = '3'
    shoulder_pieces = '4'
    armour = '5'
    gloves = '6'
    cloak = '7'
    trousers = '8'
    boots = '9'
    ring = '10'


class ArtifactTypes(Enum):
    '''Possible artifact types
    '''
    trash = 0
    primary_hand = 1
    secondary_hand = 2
    helmet = 3
    amulet = 4
    shoulder_pieces = 5
    armour = 6
    gloves = 7
    cloak = 8
    trousers = 9
    boots = 10
    ring = 11


class ArtifactRarities(Enum):
    '''Possible artifact rarities

    Rarities can be compared with each other (less/greater comparison).
    '''
    common = 0
    rare = 1
    epic = 2

    def __ge__(self, other):
        if self.__class__ is other.__class__:
            return self.value >= other.value
        return NotImplemented

    def __gt__(self, other):
        if self.__class__ is other.__class__:
            return self.value > other.value
        return NotImplemented

    def __le__(self, other):
        if self.__class__ is other.__class__:
            return self.value <= other.value
        return NotImplemented

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        return NotImplemented


class ArtifactEffects(Enum):
    '''Possible artifact effects

    True if there is some effect.
    '''
    no_effect = 666

    might = 0
    sorcery = 1
    reaction = 2
    health = 3
    intuition = 4
    cunning = 5
    astral_vessel = 6
    footman = 7
    pockets = 8

    fantastic_might = 1000
    mighty_sorcery = 1001
    superior_reaction = 1002
    incredible_health = 1003
    overintuition = 1004
    special_cunning = 1005
    big_astral_vessel = 1006
    tireless_footman = 1007
    big_pockets = 1008

    endurance = 1009
    durability = 1010
    activity = 1011
    persuation = 1012
    charm = 1013
    mental_bond = 1014
    composure = 1015
    special_aura = 1016
    quick_wittedness = 1023
    ghastly_sight = 1024

    blear_mind = 1027
    fortitude = 1030
    moral_substance = 1031

    indestructibility = 1032

    pilgrim_luck = 1028
    hero_luck = 1029

    regeneration = 1017
    last_cast = 1018
    ice = 1019
    flame = 1020
    poison = 1021
    vampirism = 1022
    accurate_attacks = 1025
    astral_barrier = 1026
    acceleration = 1033
    recklessness = 1034

    children_present = 100001

    def __bool__(self):
        return self is not ArtifactEffects.no_effect


class ArtifactState(SlotRepr):
    '''Artifact description

    Artifact description is true if artifact is not trash.

    Two artifacts are considered equal if slot where they can be equipped, their 
    physical power, their magical power, their integrity (current and maximum), 
    rarity, and effects are equal. Identifiers, hero preference rating and names 
    are not involved.
    '''
    __slots__ = ('name', 'physical_power', 'magical_power', 'type', 'integrity', 
                 'maximum_integrity', 'rarity', 'effect', 'special_effect', 
                 'preference_rating', 'equippable', 'id', 'slot')

    def __init__(self, artifact_data):
        self.name = artifact_data['name']
        '''Artifact name.'''
        self.physical_power = (
            artifact_data['power'][0] if artifact_data['power'] else None)
        '''Current artifact physical power, integer or None.'''
        self.magical_power = (
            artifact_data['power'][1] if artifact_data['power'] else None)
        '''Current artifact magical power, integer or None.'''
        self.type = ArtifactTypes(artifact_data['type'])
        '''Artifact type.'''
        if self.type is ArtifactTypes.trash:
            self.slot = None
            '''Slot for which artifact is suitable.

            May be ``None`` if artifact is trash.

            .. note:: Any valid equipment slot is true.
            '''
        else:
            self.slot = EquipmentSlots[self.type.name]
        self.rarity = (
            ArtifactRarities(artifact_data['rarity'])
            if artifact_data['rarity'] is not None
            else None
        )
        '''Artifact rarity, see :py:class:`ArtifactRarities`. May be None.'''
        self.effect = ArtifactEffects(artifact_data['effect'])
        '''Artifact effect, see :py:class:`ArtifactEffects`.'''
        self.special_effect = ArtifactEffects(artifact_data['special_effect'])
        '''Artifact special effect, see :py:class:`ArtifactEffects`.

        Special effect may be present regardless of rarity.'''
        self.preference_rating = artifact_data['preference_rating']
        '''Artifact preference rating, from the hero point of view, flot.'''
        self.equippable = artifact_data['equipped']
        '''True if artifact can be equipped.'''
        self.id = artifact_data['id']
        '''Unique artifact identifier.

        Some integer. It is not safe to assume anything else about it.'''
        self.integrity = artifact_data['integrity'][0]
        '''Current artifact integrity, integer or None.'''
        self.maximum_integrity = artifact_data['integrity'][1]
        '''Maximum artifact integrity, integer or None.'''

    def __eq__(self, other):
        for attr in ('slot', 'physical_power', 'magical_power', 'integrity',
                     'maximum_integrity', 'rarity', 'effect', 'special_effect'):
            if getattr(self, attr) != getattr(other, attr):
                return False
        return True

    def __bool__(self):
        return self.equippable


class EquipmentState(dict, SlotRepr):
    '''Hero equipment state

    Dictionary that maps values from :py:class:`EquipmentSlots` class to 
    :py:class:`ArtifactState` instances. All artifacts are also accessible as 
    attributes by names.

    If some slot is not occupied then corresponding attribute is None and 
    corresponding item is not present in dictionary.
    '''
    __slots__ = (
        '_equipment',
        'primary_hand',
        'secondary_hand',
        'helmet',
        'amulet',
        'shoulder_pieces',
        'armour',
        'gloves',
        'cloak',
        'trousers',
        'boots',
        'ring',
    )

    def __new__(cls, _):
        return dict.__new__(cls)

    def __init__(self, equipment_data):
        super(EquipmentState, self).__init__()
        self._equipment = {}
        for i in EquipmentSlots:
            try:
                artifact_data = equipment_data[i.value]
            except KeyError:
                setattr(self, i.name, None)
            else:
                artifact = ArtifactState(artifact_data)
                setattr(self, i.name, artifact)
                self[i] = artifact


class BagState(list, SlotRepr):
    '''Hero bag state

    Lists items present in hero bag.

    ``foo in BagState()`` has special meaning:

    * If ``foo`` has type :py:class:`EquipmentSlots` then it is checked whether 
      there is an artifact that fits into given slot.
    * If ``foo`` has type :py:class:`ArtifactTypes` then it is checked whether 
      there is an artifact that has given type.
    * If ``foo`` has type :py:class:`ArtifactEffects` then it is checked whether 
      there is an artifact with a given effect.
    * If ``foo`` has type :py:class:`ArtifactRarities` then it is checked 
      whether there is an artifact with a given rarity.
    * If ``foo`` has type :py:class:`ArtifactState` then it is checked whether 
      there is an equal artifact.
    * If ``foo`` has type :py:class:`int` then it is checked whether there is an 
      artifact with given identifier.
    * In all other cases ``False`` is returned.
    '''
    __slots__ = ('max_size',)

    _contains_funcs = {
        EquipmentSlots: (lambda i, key: i.slot is key),
        ArtifactTypes: (lambda i, key: i.type is key),
        ArtifactRarities: (lambda i, key: i.rarity is key),
        ArtifactEffects: (lambda i, key: (i.effect is key
                                          or i.special_effect is key)),
        ArtifactState: (lambda i, key: i == key),
        int: (lambda i, key: i.id == key),
    }

    def __new__(cls, *_):
        return list.__new__(cls)

    def __init__(self, bag_data, max_bag_size):
        super(BagState, self).__init__()
        self.max_size = max_bag_size
        '''Maximum bag size.'''
        for slot_id in sorted(bag_data.keys(), key=int):
            self.append(ArtifactState(bag_data[slot_id]))

    def __contains__(self, key):
        try:
            cfunc = self._contains_funcs[type(key)]
        except KeyError:
            return False
        else:
            return any(cfunc(i, key) for i in self)
