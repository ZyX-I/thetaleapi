'''Module that contains classes describing hero actions
'''

from .util import SlotRepr

from enum import Enum


class ActionTypes(Enum):
    '''Possible action types
    '''
    inaction = 0
    '''Hero is waiting for the next task.'''
    task = 1
    '''Hero is working on some task.'''
    travel = 2
    '''Hero is travelling between cities.'''
    battle = 3
    '''Hero is fighting with some monster.'''
    resurrection = 4
    '''Hero is dead and is being resurrected.'''
    city = 5
    '''Hero is doing some actions in city.'''
    relaxation = 6
    '''Hero is resting.'''
    equipment = 7
    '''Hero is doing something with his equipment.

    .. todo: Better description.'''
    trade = 8
    '''Hero is selling monster drop or former equipment.'''
    travel_near_city = 9
    '''Hero is travelling around city.

    This normally happens as a part of his task.'''
    energy_restoration = 10
    '''Hero is restoring custodian energy.'''
    action_without_effect = 11
    '''Hero is doing some action which does not have in-game effect.

    .. todo: What is this?'''
    proxy_hero_interaction = 12
    '''Proxy action for hero interactions.

    .. todo: ???'''
    pvp_1x1 = 13
    '''Hero is fighting with another hero.'''
    test = 14
    '''Action used for testing something.'''
    companion_care = 15
    '''Hero is taking care of his companion.'''


class Action(SlotRepr):
    '''Class describing hero action
    '''
    __slots__ = ('type', 'description', 'info_link', 'progress')

    def __init__(self, action_data):
        self.type = ActionTypes(action_data['type'])
        '''Action type, see :py:class:`ActionTypes`.'''
        self.description = action_data['description']
        '''Human-readable action description.'''
        self.info_link = action_data['info_link']
        '''URL providing additional information or ``None``.'''
        self.progress = action_data['percents'] / 100
        '''Action progress (in parts of 1).'''
