'''Module that contains classes describing game state
'''

from .util import SlotRepr
from .time import TheTaleTime, TheTaleDate

from enum import Enum


class GameMode(Enum):
    '''Possible game modes
    '''
    pve = 'pve'
    '''Regular mode.'''
    pvp = 'pvp'
    '''Arena mode.'''


class GameServerState(Enum):
    '''Possible game states

    This class is true if game state is ``running``.
    '''
    stopped = 0
    '''Game is stopped.'''
    running = 1
    '''Game is running.'''

    def __bool__(self):
        return self is GameServerState.running


class TurnInfo(SlotRepr):
    '''Turn description
    '''
    __slots__ = ('number', 'date', 'time')

    def __init__(self, turn_data):
        self.number = turn_data['number']
        '''Turn number.

        Some internal integer. Is always incrementing.
        '''
        self.date = TheTaleDate(turn_data['verbose_date'])
        '''Internal game date, see :py:class:`thetale.time.TheTaleDate`.'''
        self.time = TheTaleTime(turn_data['verbose_time'])
        '''Internal game time, see :py:class:`thetale.time.TheTaleTime`.'''


class GameState(SlotRepr):
    '''Game description

    This class is true if game is running.
    '''
    __slots__ = ('mode', 'state', 'turn', 'pvp_state')

    def __init__(self, data):
        self.mode = GameMode(data['mode'])
        '''Current game mode, see :py:class:`GameMode`.'''
        self.state = GameServerState(data['game_state'])
        '''Current game state, see :py:class:`GameServerState`.'''
        self.turn = TurnInfo(data['turn'])
        '''Information about current turn, see :py:class:`TurnInfo`.'''

    def __bool__(self):
        return bool(self.state)


def int_or_str(i):
    '''Convert string to integer or return it unchanged if conversion fails

    Used for such weird versions as ``….april_1``.

    :param str i:
        Converted string.

    :return: Integer or converted string unchanged.
    '''
    try:
        return int(i)
    except ValueError:
        return i


class GameVersionInfo(tuple, SlotRepr):
    '''Game version info, similar to :py:attribute:`sys.version_info`
    '''
    _slots = ('major', 'minor', 'micro')

    def __new__(cls, game_version):
        assert game_version[0] == 'v'
        args = [int_or_str(i) for i in game_version[1:].split('.')]
        return tuple.__new__(cls, args)

    def __init__(self, _):
        super(GameVersionInfo, self).__init__()
        self.major = self[0]
        '''Major version number, integer.'''
        self.minor = self[1]
        '''Minor version number, integer.'''
        self.micro = self[2]
        '''Patch version number, integer.'''
