thetale package
===============

Submodules
----------

.. toctree::

   thetale.account
   thetale.cards
   thetale.companion
   thetale.equipment
   thetale.game
   thetale.hero
   thetale.info
   thetale.relations
   thetale.request
   thetale.session

Module contents
---------------

.. automodule:: thetale
    :members:
    :no-undoc-members:
    :inherited-members:
